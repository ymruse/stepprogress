# stepprogress

#### 介绍
基于layui实现的简单步骤进度条，提供api操作进度步骤进度条前进、后退、移动到指定位置等。

#### 软件架构
基于layui实现的步骤进度条，用于图形化表示需要分步骤进行的处理的进度。

#### 例子截图
![输入图片说明](sample.png)

#### 安装教程
下载stepprogress目录及目录内的js、css文件，放在web工程的js相关文件static目录即可。

#### 使用说明
下面是对应 例子截图 的代码。详细请参考sample文件。
```
layui.config({
    base:'./stepprogress/'
}).use(['stepprogress', 'layer'], function(){
	var stepprogress = layui.stepprogress;
	var $ = layui.$;
	var layer = layui.layer;
	
	renderStepProgress();
	
	// 绑定进度变化事件
	stepprogress.on('change(stepProgressBar)', function(options){
		console.log(options);
	});
	
	// 渲染步骤进度条
	function renderStepProgress() {
		let stepItems = [{
			title: '开始',
			code: '01'
		},{
			title: '做成基础数据',
			code: '02'
		},{
			title: '手工调整',
			code: '03'
		},{
			title: '生成报表',
			code: '90'
		}];
		stepprogress.render({
			elem: '#stepProgressBar',
			stepItems: stepItems,
			position: 0
		});
	}
});

```

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
